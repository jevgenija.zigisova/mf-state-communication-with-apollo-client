import { gql } from '@apollo/client';

export default gql`
  query global_settings($locale: String!) {
    global_settings(uid: "bltcadf67446f20fc0b", locale: $locale) {
      site_name
      site_logoConnection {
        edges {
          node {
            url
          }
        }
      }
    }
  }
`;
