import { countVar } from 'cart/schema';
import createAddProduct from './addProduct';

const mutations = {
  addProduct: createAddProduct(countVar),
};

export default mutations;
