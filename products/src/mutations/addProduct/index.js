export default function createAddProduct(countVar) {
  return () => {
    const value = countVar();
    countVar(value + 1);
  };
}
