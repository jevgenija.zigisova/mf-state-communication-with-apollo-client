import { makeVar } from '@apollo/client';

export const countVar = makeVar(0);

export const querySchema = {
  count: {
    read() {
      return countVar();
    },
  },
};
