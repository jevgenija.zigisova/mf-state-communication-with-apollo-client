import React from 'react';
import mutations from '../../mutations';

const SelectProduct = () => {
  return (
    <button type="submit" onClick={mutations.addProduct}>
      Select product
    </button>
  );
};

export default SelectProduct;
