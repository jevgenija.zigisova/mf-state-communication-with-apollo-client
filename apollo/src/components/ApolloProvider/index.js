import React from 'react';
import { ApolloProvider } from '@apollo/client';
import client from '../../graphqlClient';

// eslint-disable-next-line react/prop-types
const ApolloComponent = ({ children }) => (
  <ApolloProvider client={client}>{children}</ApolloProvider>
);

export default ApolloComponent;
