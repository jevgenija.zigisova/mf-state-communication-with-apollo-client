import React from 'react';
import { useQuery, gql } from '@apollo/client';
import styles from './styles.scss';
import query from './query';

const COUNT = gql`
  query getCount {
    count @client
  }
`;

const Header = () => {
  const { data, loading, error } = useQuery(query, {
    variables: {
      locale: 'en-us',
    },
  });

  const { data: countData } = useQuery(COUNT);

  if (loading || error) return null;

  const logo = data?.global_settings?.site_logoConnection?.edges[0]?.node;

  return (
    <div className={styles.header}>
      <img className={styles.logo} src={logo.url} alt={data?.global_settings?.site_name ?? ''} />
      <span>
        Cart:
        {countData?.count}
      </span>
    </div>
  );
};

export default Header;
