import React from 'react';
import ApolloProvider from 'apollo/ApolloProvider';
import Header from 'components/Header';

const Home = () => (
  <ApolloProvider>
    <Header />
  </ApolloProvider>
);

export default Home;
