import React from 'react';
import ApolloProvider from 'components/ApolloProvider';
import Header from 'cart/Header';
import SelectProduct from 'products/SelectProduct';

const Home = () => (
  <ApolloProvider>
    <Header />
    <SelectProduct />
  </ApolloProvider>
);

export default Home;
