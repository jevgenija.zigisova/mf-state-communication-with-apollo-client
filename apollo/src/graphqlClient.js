import { ApolloClient, ApolloLink, HttpLink, InMemoryCache, concat } from '@apollo/client';
import { querySchema } from 'cart/schema';

const CS_API_KEY = 'bltaad01d1a2ba34de9';
const CS_ENVIRONMENT = 'staging';
const CS_DELIVERY_TOKEN = 'csb48b5952ed8fd715c903121a';

const httpLink = new HttpLink({
  uri: `https://eu-graphql.contentstack.com/stacks/${CS_API_KEY}?environment=${CS_ENVIRONMENT}`,
});
const authMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      access_token: CS_DELIVERY_TOKEN,
    },
  });

  return forward(operation);
});
const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        ...querySchema,
      },
    },
  },
});

const client = new ApolloClient({
  cache,
  connectToDevTools: true,
  link: concat(authMiddleware, httpLink),

  defaultOptions: {
    query: {
      fetchPolicy: 'cache-and-network',
    },
  },
});

export default client;
